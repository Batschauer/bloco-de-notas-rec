package com.example.aluno.sharepreferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.logging.SocketHandler;

public class Notas {
    private final String NOTAS_FINAL = "Notas";
    private final String NOTA_KEY = "nota";
    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor editor;

    public Notas(Context c) {
        sharedPreferences = c.getSharedPreferences(NOTAS_FINAL, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    public void SalvarNotas (String s){

        editor.putString(NOTA_KEY, s);
        editor.commit();
    }

    public String RecuperarNotas(){
        return sharedPreferences.getString(NOTA_KEY, "");
    }
}
